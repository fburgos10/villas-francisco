window.addEventListener('load', ()=>{
console.log('[Test] estatus module.');  

// Enviar Formulario
const form = document.querySelector('#formTipos');
const eventButton =  document.getElementById("SaveTipo");

  eventButton.addEventListener('click', (e)=>{
      console.log('[Test] Guardar pulsado.'); 
      e.preventDefault();
      let data = new FormData(form);
      let descripcion = document.getElementById("descripcion").value;
      let clase_estatus = document.getElementById("clase_estatus").value;
     
      if (descripcion.length < 3) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alerta!</strong> Campo descripción esta vacio' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    //131-94679-8
    } else {
      axios({
        method  : 'post',
        url : 'api/estatus.php',
        data: {
          descripcion: descripcion,
          clase_estatus: clase_estatus
        }
      })
      .then((res)=>{
        console.log(res);
        $('#alertPass').html('');
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        $('#modalTiposEstatus').modal('hide');
        $('.form-control').val('')
        alerty('Empresa creada con exitos', 'success')
        
      })
      .catch((err) => {throw err});
    }
  });

});

// ELiminar tipos
function deleteTipos(id){
  axios({
    method  : 'post',
    url : 'api/deleteEstatus.php',
    data: {
      id: id,
    },
    }).then((res)=>{
        console.log(res);
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        alerty('Estatus eliminado', 'danger');
    }).catch((err) => {throw err});
}
