<?php

/*require_once("config/db.php");
require_once("config/conexion.php");*/
require_once("config/database.php");
require_once("classes/Login.php");

$login = new Login();
@$nivel = $_SESSION['nivel'];

if ($login->isUserLoggedIn() == true && $nivel == 1) {
    header("location: home?lang=en");
} else if ($login->isUserLoggedIn() == true && $nivel == 2) {
    header("location: dashboard?lang=en");
} else if ($login->isUserLoggedIn() == true && $nivel == 3) {
    header("location: ordenes?lang=en");
} else {
?>
<!DOCTYPE html>
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>.:Luxury:.</title>
  <meta name="viewport" content="width=device-width, user-scalable=no">
  <link rel="icon" type="image/png" href="img/2GoTabIcon.png">
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/login.css">
</head>
<style>
  body
  {
    background: url('shutterstock_1315460261.jpg');
    -webkit-background-size: cover;
    
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }
  .wrapper
  {
    /*background-color: rgba(0,0,0,0.5);*/
    background-color: #851079;
  }
</style>
<body>
<script>
</script>
<div class="wrapper  fadeIn">
  <div class="formContent" style="background-color:white">
    <div class="">
    <br>
    <img src="img/logo.png" width="200px" width="200px" style="margin-top: 10px;margin-bottom: 10px;"  alt="User Icon" />
    </div>
    <?php
      if (isset($login)) {
          if ($login->errors) {
    ?>
      <div class="alert alert-danger alert-dismissible" role="alert" style="width: 80%;margin-left: 10%;margin-right: 10%">
          <strong>Error!</strong> 
    <?php 
      foreach ($login->errors as $error) {
        echo $error;
      }
    ?>
      </div>
    <?php }
       if ($login->messages) {
    ?>
      <div class="alert alert-success alert-dismissible" role="alert" style="width: 80%;margin-left: 10%;margin-right: 10%">
          <strong>Aviso!</strong>
    <?php
      foreach ($login->messages as $message) {
          echo $message;
        }
    ?>
      </div> 
    <?php } } ?>
    <br/>
    <form action="login.php" name="loginform" autocomplete="off" role="form" method="post">
      <input type="text" name="user_name" id="user" class="fadeIn second" placeholder="Usuario">
      <input type="password" name="user_password" id="user_password" class="fadeIn third" placeholder="Contraseña">
      <span><img src="img/view.png" alt="" style="width: 6%;position: absolute;right: 45px;bottom: 187px; cursor: pointer"></span>
      <input type="submit"  id="IngresoLog" name="login" class="fadeIn fourth" value="Iniciar Sesión" style="background-color: #f50000;margin-top: 15px; cursor: pointer">
    </form>

    <div class="row">
      <div class="col" style="text-align: center;margin: 20px;">
        <a href="reset-pass">¿ Olvidaste tu contraseña ?</a>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/js_login.js"></script>
</body>
</html>
<?php } ?>