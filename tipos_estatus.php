<!--- Home --->
<?php 
include_once('header/navbar.php');
include_once 'config/database.php';
include_once('class/tipos_estatus.php');
include_once('modals/modaltiposestatus.php');
$database = new Database();
$db = $database->getConnection();
?>
<div class="content">

  <div class="container-fluid">

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <?php include_once('modals/modaltiposestatus.php'); ?>
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Tipos Tareas
               <button type="button" style="float: right;" class="btn btn-default" data-toggle="modal" data-target="#modalTiposEstatus">Nuevo</button>
            </h4>
            <p class="card-category"> Tipos de tareas registradas</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table" id="ReloadTable">
                <thead class=" text-primary">
                  <tr>
                    <td>Código</td>
                    <td>Descripción</td>
                    <td>Clase estatus</td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  <?php
                   $items = new TiposestatusClass($db);
                   $stmt = $items->getTipos();
                   $itemCount = $stmt->rowCount();

                   if($itemCount > 0){
                     while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
                      extract($row);
                  ?>
                  <tr>
                    <td><?php print($id); ?></td>
                    <td><?php print($descripcion); ?></td>
                    <td><?php print($clase_estatus); ?></td>
                    <td>
                     <button type="button" class="btn btn-danger" onclick="deleteTipos('<?php print($id); ?>')">
                        <span class="material-icons">delete</span>
                      </button>
                    </td>
                  </tr>
                  <?php } 
                    }else{
                      echo "<tr>";
                      echo "<td colspan='5' style='font-size:20px; font-weight: 400; height: 100px;' align='center'>No hay registros disponibles</td>";
                      echo "</tr>";
                    } ?>   
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/tiposestatus.js"></script>
<?php 
include_once('header/footer.php');
?>