<div class="modal" tabindex="-1" id="modalTiposTareas" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Nuevo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="alertPass"></div>   
      	<form id="formTipos">
      		<div class="row">
      			<div class="col">
      				<input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción Tipo tarea">
      			</div>
        		</div>

      	</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="SaveTipo">
        	<span class="material-icons" style="vertical-align: middle;display: inline-block;">save</span>
            Guardar
        </button>
        <button type="button" class="btn btn-secondary" style="vertical-align: middle;display: inline-block;" data-dismiss="modal">
        	<span class="material-icons">clear</span>
            Close
        </button>
      </div>
    </div>
  </div>
</div>