window.addEventListener('load', ()=>{
console.log('[Test] Address module.');  

// Enviar Formulario
const form = document.querySelector('#formAddress');
const eventButton =  document.getElementById("SaveAddress");

  eventButton.addEventListener('click', (e)=>{
      console.log('[Test] Guardar pulsado.'); 
      e.preventDefault();
      let data = new FormData(form);
      let country = document.getElementById("country").value;
      let direccion = document.getElementById("direccion").value;
      let postal_code = document.getElementById("postal_code").value;
      let latitud = document.getElementById("latitud").value;
      let longitud = document.getElementById("longitud").value;
      let ciudad = document.getElementById("ciudad").value;
      let idCustomer = document.getElementById("idCustomer").value;

    
    if (country.length < 1) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Campo pais esta vacio' +
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else if (direccion.length < 3) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Campo direccion esta vacio'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else if (postal_code.length < 3) {
      $('#alertPass').html('<div class="alert alert-warning" role="alert">' +
        '<strong>Alert!</strong> Campo zipcode esta vacio'+
        '<button type="button" class="close" data-dismiss="alert" aria-label="Close" style="position: sticky;">' +
        '<span aria-hidden="true">&times;</span>' +
        '</button></div>');

    } else {
      axios({
        method  : 'post',
        url : 'api/createAddress.php',
        data: {
          country: country,
          direccion: direccion,
          postal_code: postal_code,
          latitud: latitud,
          longitud:  longitud,
          ciudad: ciudad,
          id: idCustomer
        },
      })
      .then((res)=>{
        console.log(res);
        console.log(data);
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        $('#modalAddress').modal('hide');
        $('.form-control').val('')
        $('#country').val('Republica Dominicana');
        alerty('Direccion agregada con exitos', 'success');

      })
      .catch((err) => {throw err});
    }
  });

});

// ELiminar Cliente
function deleteAddress(id){
  axios({
    method  : 'post',
    url : 'api/deleteAddress.php',
    data: {
      id: id,
    },
    }).then((res)=>{
        console.log(res);
        $("#ReloadTable").load(location.href + " #ReloadTable>*", "");
        alerty('Direccion eliminada con exitos', 'danger');
    }).catch((err) => {throw err});
}
