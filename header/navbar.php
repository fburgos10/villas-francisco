<!doctype html>
<html lang="en">

<head>
  <title>.:Destino RD:.</title>
  <meta charset="utf-8">
  <link rel="icon" type="image/png" href="img/logo.png">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link href="assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"> </script>
  <!-- Alertify -->
  <script src="js/alertify.js"></script>
  <script src="js/alertify.min.js"></script>
  <link href="css/alertify.css" rel="stylesheet">
  <link href="css/alertify.min.css" rel="stylesheet">
</head>
<script type="text/javascript">
  function alerty(message, type){
    $.notify({
      icon: "<span class='material-icons'>notifications</span>",
      message: message
    }, {
      type: type,
      timer: 2000,
      placement: {
        from: 'top',
        align: 'center'
      }
    });
  }
</script>
<body>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white">
      <div class="logo">
        <a href="#" class="simple-text logo-normal">
          <img src="./img/logo.png" style="width: 150px;height: 130px;">
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="index">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="index">
              <i class="material-icons">dashboard</i>
              <p>Facturacion</p>
            </a>
          </li>
           <li class="nav-item">
            <a class="nav-link" href="index">
              <i class="material-icons">dashboard</i>
              <p>Clientes</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index">
              <i class="material-icons">dashboard</i>
              <p>Pagos</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index">
              <i class="material-icons">dashboard</i>
              <p>Reportes</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="index">
              <i class="material-icons">dashboard</i>
              <p>Usuarios</p>
            </a>
          </li>
          <!-- your sidebar here -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;">Dashboard</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="javascript:;">
                  <i class="material-icons">notifications</i> Notifications
                </a>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->