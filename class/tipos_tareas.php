<?php
    class TipostareasClass{

        // Connection
        private $conn;

        // Table
        private $db_table = "tipos_tareas";

        // Columns
        public $id;
        public $descripcion;
        public $fecha_hora;


        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getTipos(){
            $sqlQuery = "SELECT id, descripcion, fecha_hora FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createTipo(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        descripcion = :descripcion, 
                        fecha_hora = :created";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->descripcion=htmlspecialchars(strip_tags($this->descripcion));
            $this->created=htmlspecialchars(strip_tags($this->created));
        
            // bind data
            $stmt->bindParam(":descripcion", $this->descripcion);
            $stmt->bindParam(":created", $this->created);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // UPDATE
        public function getSingleBussines(){
            $sqlQuery = "SELECT
                        id, 
                        name, 
                        phone, 
                        rnc, 
                        address, 
                        created
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->name = $dataRow['name'];
            $this->phone = $dataRow['phone'];
            $this->rnc = $dataRow['rnc'];
            $this->address = $dataRow['address'];
            $this->created = $dataRow['created'];
        }        

        // UPDATE
        public function updateBussines(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        name = :name, 
                        phone = :phone, 
                        rnc = :rnc, 
                        address = :address, 
                        created = :created
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->name=htmlspecialchars(strip_tags($this->name));
            $this->phone=htmlspecialchars(strip_tags($this->phone));
            $this->rnc=htmlspecialchars(strip_tags($this->rnc));
            $this->address=htmlspecialchars(strip_tags($this->address));
            $this->created=htmlspecialchars(strip_tags($this->created));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":name", $this->name);
            $stmt->bindParam(":phone", $this->phone);
            $stmt->bindParam(":rnc", $this->rnc);
            $stmt->bindParam(":address", $this->address);
            $stmt->bindParam(":created", $this->created);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteTipos(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>

