<?php
    class TiposestatusClass{

        // Connection
        private $conn;

        // Table
        private $db_table = "estatus";

        // Columns
        public $id;
        public $descripcion;
        public $clase_estatus;


        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getTipos(){
            $sqlQuery = "SELECT id, descripcion, clase_estatus FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createTipo(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        descripcion = :descripcion, 
                        clase_estatus = :clase_estatus";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->descripcion=htmlspecialchars(strip_tags($this->descripcion));
            $this->created=htmlspecialchars(strip_tags($this->created));
        
            // bind data
            $stmt->bindParam(":descripcion", $this->descripcion);
            $stmt->bindParam(":clase_estatus", $this->clase_estatus);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteTipos(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>

