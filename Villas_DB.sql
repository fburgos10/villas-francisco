/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.5.5-10.4.14-MariaDB : Database - villas_francisco
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`villas_francisco` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `villas_francisco`;

/*Table structure for table `areas_zonas` */

DROP TABLE IF EXISTS `areas_zonas`;

CREATE TABLE `areas_zonas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `ubicacion` varchar(255) DEFAULT NULL,
  `id_villa` int(10) DEFAULT NULL,
  `tipo_area` varchar(50) DEFAULT NULL,
  `fecha_hora` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `areas_zonas` */

/*Table structure for table `bussines` */

DROP TABLE IF EXISTS `bussines`;

CREATE TABLE `bussines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `phone` varchar(13) NOT NULL,
  `rnc` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `bussines` */

insert  into `bussines`(`id`,`name`,`phone`,`rnc`,`address`,`created`) values (5,'Sprite LLC','8528965789',12345678,'Calle 5','2021-06-04 13:33:29'),(7,'Coca Cola','8097896549',123456789,'calle 18','2021-06-04 13:44:00'),(9,'Pepsi','2756573563',784466465,'calle 20','2021-06-04 13:45:08'),(10,'Cola Real','4545456456',789658236,'calle 85','2021-06-04 13:53:31'),(12,'Lays','888-888-8888',545454546,'calee 4','2021-06-04 14:05:36'),(13,'Cacaguate','809-854-5698',415131313,'Calle 25','2021-06-04 18:59:03');

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `apellido` varchar(25) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `customer` */

insert  into `customer`(`id`,`name`,`apellido`,`email`,`address`,`phone`,`id_company`,`created`) values (1,'Fernando','Burgos','fernandohbd10@gmail.com','Calle 45','809-852-7896',5,'2021-06-04 15:32:37'),(2,'Blacdimir','Rosario','blacdimirr@gmail.com','callle 4','809-564-5669',5,'2021-06-04 15:32:37'),(3,'Hector','Morales','h.morales@gmail.com','calle 89','829-545-5556',5,'2021-06-04 15:36:57'),(5,'Carlos','Rubio','carlos.rubio@mail.com','calle 345','809-854-5665',9,'2021-06-04 16:13:06'),(6,'Mario','Casas','m.casas@gmail.com','calle 34','829-869-5489',9,'2021-06-04 16:14:43'),(7,'Roberto','Fernandez','fernandohbd100@gmail.com','calle 985','809-556-5555',13,'2021-06-04 18:59:39');

/*Table structure for table `detalle_tarea` */

DROP TABLE IF EXISTS `detalle_tarea`;

CREATE TABLE `detalle_tarea` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id_tarea` int(255) DEFAULT NULL,
  `estatus` int(5) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `fecha_hora` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `detalle_tarea` */

/*Table structure for table `direcciones` */

DROP TABLE IF EXISTS `direcciones`;

CREATE TABLE `direcciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(200) DEFAULT NULL,
  `zipcode` varchar(6) DEFAULT NULL,
  `ciudad` varchar(20) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `pais` varchar(50) DEFAULT NULL,
  `latitud` varchar(50) DEFAULT NULL,
  `longitud` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

/*Data for the table `direcciones` */

insert  into `direcciones`(`id`,`direccion`,`zipcode`,`ciudad`,`id_customer`,`pais`,`latitud`,`longitud`,`created`) values (3,'Calle Mauricio Baez 201, Santo Domingo, República Dominicana','10413','Ensanche La Fé',NULL,'Republica Dominicana','18.497043728464316','-69.96696779570313','2021-06-04 18:20:28'),(4,'Sambil, Av. John F. Kennedy, Santo Domingo 10413, República Dominicana','10413','Santo Domingo',1,'Republica Dominicana','18.4880429','-69.96845669999999','2021-06-04 18:25:16'),(5,'Juan Pablo Duarte L1/L2, Av. John F. Kennedy, Santo Domingo 10122, República Dominicana','10122','Centro Olimpico',1,'Republica Dominicana','18.49769490064501','-69.97932741484375','2021-06-04 18:26:36'),(6,'Calle Moca 76a, Santo Domingo 10413, República Dominicana','10413','Villa Juana',1,'Republica Dominicana','18.50355533884775','-69.90104982695313','2021-06-04 18:26:48'),(7,'Calle Sumner Wells 24, Santo Domingo 10413, República Dominicana','10413','Villa Juana',5,'Republica Dominicana','18.49118306743597','-69.93126222929688','2021-06-04 18:27:07'),(8,'Calle José Martí 172, Santo Domingo 10306, República Dominicana','10306','Mejoramiento Social',7,'Republica Dominicana','18.48643830624367','-69.89506704763794','2021-06-04 19:00:16'),(9,'Expreso V Centenario 17, Santo Domingo 10308, República Dominicana','10308','Villa Juana',7,'Republica Dominicana','18.487252321472816','-69.90414364294433','2021-06-04 19:00:22'),(10,'Calle Francisco Villaespesa 33, Santo Domingo 10413, República Dominicana','10413','Villa Juana',7,'Republica Dominicana','18.487577926481794','-69.90927202658081','2021-06-04 19:00:26'),(11,'Calle Lope De Vega 140, Santo Domingo, República Dominicana','10413','Viejo Arroyo Hondo',1,'Republica Dominicana','18.485973422655746','-69.94842836699219','2021-07-17 20:13:45'),(12,'Calle 26, Santo Domingo Este 11605, República Dominicana','11605','Santo Domingo Este',1,'Republica Dominicana','18.482391699999994','-69.83925173125','2021-07-17 20:13:55');

/*Table structure for table `estatus` */

DROP TABLE IF EXISTS `estatus`;

CREATE TABLE `estatus` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `tipo_estatus` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `estatus` */

/*Table structure for table `tarea_general` */

DROP TABLE IF EXISTS `tarea_general`;

CREATE TABLE `tarea_general` (
  `id_tarea` int(255) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `id_empleado` int(50) DEFAULT NULL,
  `id_tipotarea` int(50) DEFAULT NULL,
  `id_creador` int(50) DEFAULT NULL,
  `id_villa` int(50) DEFAULT NULL,
  `id_area` int(50) DEFAULT NULL,
  `estatus` int(5) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `clase_tarea` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_tarea`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `tarea_general` */

/*Table structure for table `tipos_tareas` */

DROP TABLE IF EXISTS `tipos_tareas`;

CREATE TABLE `tipos_tareas` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `fecha_hora` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tipos_tareas` */

insert  into `tipos_tareas`(`id`,`descripcion`,`fecha_hora`) values (1,'LAVADO DE PISCINA','2020-10-01 00:00:00'),(2,'MANTENIMIENTO PLANTA','2020-10-01 00:00:00'),(3,'rueba','2021-10-01 06:09:55');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index',
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique',
  `user_password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format',
  `restaurante` int(11) DEFAULT NULL,
  `user_email` varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique',
  `date_added` datetime NOT NULL,
  `nivel` int(11) DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cedula` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hidden` int(11) DEFAULT 1,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `indx_users_restaurante` (`restaurante`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data';

/*Data for the table `users` */

insert  into `users`(`user_id`,`firstname`,`lastname`,`user_name`,`user_password_hash`,`restaurante`,`user_email`,`date_added`,`nivel`,`estado`,`cedula`,`telefono`,`hidden`,`token`,`fecha_nacimiento`) values (1,'Fernando','Burgos','fernando','$2y$10$9Ua8Y3rRKP0x.O/aS0ERDes/10xLjoxZGu2frUeQkv1pkR6N1yc0a',0,'fernandohbd10@gmail.com','2019-05-09 00:00:00',1,'enable','402-0000000-0','809-000-0000',1,'fzHpgrmQoIY:APA91bH2ddGALl0QRVXncn3PeiuCyCUuAn1q7MFUenpoB3_Cey4ZSPGLEJdlPMkVv_w-scnwwxx6U__y4QsC4V3XkaW7FRkfgPC5xqnqG1y5m-X0Woo3Ty5wPHQAmgnxL_HjKdwRAHRs','1988-01-30'),(51,'Prueba UNO','UNO','PRUEBAUNO','$2y$10$R.zACzo3lBirHTB6ETV2YeBs1WvsOIZTYFEjMHX11q6vF/JzB.Q3C',0,'ASD@SDF.COM','2021-03-02 00:00:00',3,'enable',NULL,NULL,1,NULL,NULL),(59,'Felix','Carvajal','FelixC','$2y$10$9Ua8Y3rRKP0x.O/aS0ERDes/10xLjoxZGu2frUeQkv1pkR6N1yc0a',40,'felix.carvajal@utreee.com','2021-03-18 00:00:00',2,'enable',NULL,NULL,1,NULL,NULL),(72,'user010','name','user01','$2y$10$FFuM.ld/UiT7uq6mTHv.u.GUbc3IoBC4zq44SIljJA7D.Si/pZQ5u',40,'user01@gmail.com','2021-05-05 00:00:00',3,'enable',NULL,NULL,1,NULL,NULL),(73,'user003','name','user0030','$2y$10$oNxLc/kCf4vMQUNQMXzaJ.U3U2Ktbgz2plvDEPwrsuIx4hk6YnqMO',40,'user03@gmail.com','2021-05-05 00:00:00',3,'enable',NULL,NULL,1,NULL,NULL),(76,'Deana','Deana','deana','$2y$10$cYbC3PaOWi3dLe0Thi88Guuj3WJVjQQfXAzY/kEkBnG2lSJ49lahK',59,'djmnza85@gmail.com','2021-05-06 00:00:00',2,'enable',NULL,NULL,1,NULL,NULL),(77,'Deanita','Deanita','Deanita','$2y$10$PGWUvRwtDGwb3B/O0YSkluadKgaOjTGfvZVg8h36t3U2Jus.maTaG',59,'djmn999za85@gmail.com','2021-05-06 00:00:00',2,'enable',NULL,NULL,1,NULL,NULL),(78,'DIANA','JIMENEZ','DJIMENZ','$2y$10$m/bipA/OW9jLRQ0dHkaTO.z1F49kluF7MSii6IkTmSCP3GHWg2qF2',0,'diana.jimenez@utreee.com','2021-05-06 00:00:00',1,'enable',NULL,NULL,1,NULL,NULL);

/*Table structure for table `villas` */

DROP TABLE IF EXISTS `villas`;

CREATE TABLE `villas` (
  `id_villa` int(255) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lng` varchar(20) DEFAULT NULL,
  `adulto_solamente` int(2) DEFAULT NULL,
  `limite_personas` int(20) DEFAULT NULL,
  `invitados_maximo` int(20) DEFAULT NULL,
  PRIMARY KEY (`id_villa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `villas` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
